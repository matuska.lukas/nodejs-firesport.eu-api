const dateFormat = require('dateformat');
module.exports = {
    // location
    protocol: 'http',
    url: 'localhost',
    port: 3002,

    // full url
    fullUrl: this.protocol + '://' + this.url + (String(this.port).length > 0 ? ':' + this.port : ''),
};