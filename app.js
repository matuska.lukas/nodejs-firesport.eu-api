/**
 * The entry point of the Firesport.eu API app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

// config as global variable
global.CONFIG = require('./config');

// load the server plugin (Express)
const express = require('express');
const app = express();

// load some libraries
const moment = require('moment');
const path = require('path');
const bodyparser = require('body-parser');


// set extended urlencoded to true (post)
app.use(bodyparser.urlencoded({extended: true}));

// set up views directory and the rendering engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// set serving static files from the static dir
app.use(express.static(path.join(__dirname, 'static')));

/**
 * Routers
*/
const apiRouter = require('./routes/api');
app.use('/api', apiRouter);

// run the server
app.listen(CONFIG.port, () => {
    console.log(moment().format('YYYY-MM-DD HH:mm:ss') + ' Listening on port ' + CONFIG.port + ' (Firesport.eu API Node.js <3)');
});