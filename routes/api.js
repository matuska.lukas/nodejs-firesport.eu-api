/**
 * The entry router of the app
 * @author Lukas Matuska (lukynmatuska@gmail.com)
 * @version 1.0
 * @see https://lukasmatuska.cz/
 */

/**
 * Express router API
 */
const router = require('express').Router();

/**
 * Libraries
 */
const moment = require('moment');
const numberFormat = require('../libs/numberFormat');

/**
 * Controllers
 */
const attackController = require('../controllers/attack');

/**
 * Partials methods
 */
//const partials = require('./partials');


/**
 * Routes
 */

// set local variables
router.all('/*', (req, res, next) => {
    res.locals = {
        currentPath: req.originalUrl,
        moment: moment,
        numberFormat: numberFormat,
    };

    // move to the next route
    next();
});

router.get('/newAttack', (req, res) => {
    attackController.add(req, res);
});

/**
 * Export the router
 */
module.exports = router;